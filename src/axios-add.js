import axios from 'axios';

const instance = axios.create({
        baseURL: 'https://homework-66.firebaseio.com/'
});

export default instance;